/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import entities.Transport;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import services.TransportService;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class AjoutTransportController implements Initializable {
    @FXML
    private AnchorPane FctScn;
    @FXML
    private Pane lol;
    @FXML
    private TextField companie;
    @FXML
    private TextField type;
    @FXML
    private TextField horaire;
    @FXML
    private TextField taller;
    @FXML
    private Button helaaffich;
    @FXML
    private Button ajout;
    @FXML
    private TextField tretour;
    @FXML
    private TextField prix;

    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void AfficherTransport(ActionEvent event) {
            try {
                FctScn.getChildren().clear();
   FctScn.getChildren().add(FXMLLoader.load(getClass().getResource("AfficherTransport.fxml")));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    @FXML
    private void AjouterTransport (ActionEvent event) {
        TransportService ts = new TransportService();
        System.out.println(companie.getText()+" "+type.getText()+" "+horaire.getText()+" "+taller.getText()+" "+tretour.getText()+" "+Integer.parseInt(prix.getText()));
     
        Transport t = new Transport(companie.getText(),type.getText(),horaire.getText(),taller.getText(),tretour.getText(),Integer.parseInt(prix.getText()));
        try {
            ts.AjouterTransport(t);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


   

   
   
    
}
