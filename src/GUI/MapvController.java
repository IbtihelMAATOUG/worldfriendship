/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.JavascriptObject;
import com.lynden.gmapsfx.javascript.object.Animation;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.service.geocoding.GeocodingService;
import com.lynden.gmapsfx.service.geocoding.GeocodingServiceCallback;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class MapvController  implements Initializable ,MapComponentInitializedListener{
    @FXML
    private GoogleMapView mapview;
private GeocodingService geocodingService;
    private GoogleMap googlemap;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       mapview.addMapInializedListener(this); 
    }    

    @Override
    public void mapInitialized() {
       geocodingService = new GeocodingService();
        GeocodingServiceCallback callback = geocodingService.callback;
        //System.out.println(callback);
       MapOptions options = new MapOptions();
        
        options.center(new LatLong(47.606189, -122.335842))
                .zoomControl(true)
                .zoom(12).rotateControl(false)
                .overviewMapControl(false).mapMaker(true)
                .mapType(MapTypeIdEnum.ROADMAP);
        
       
        
        googlemap = mapview.createMap(options);
        
         MarkerOptions markerOptions = new MarkerOptions();
        LatLong markerLatLong = new LatLong(47.606189, -122.335842);
        markerOptions.position(markerLatLong)
                .title("My new Marker")
                .animation(Animation.DROP)
                .visible(true);
        
                Marker myMarker = new Marker(markerOptions);

                googlemap.addMarker(myMarker);
    }
    
}
