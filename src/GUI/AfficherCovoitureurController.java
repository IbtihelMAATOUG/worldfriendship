/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import config.connectionDB;
import entities.Covoitureur;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import services.CovoitureurServices;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class AfficherCovoitureurController implements Initializable {
    @FXML
    private TableView<Covoitureur> TableNut;
    private TableColumn<?, ?> htreg;
    private TableColumn<?, ?> hpoids;
    @FXML
    private Button nutremove;
    @FXML
    private Button nutback;
    @FXML
    private TableColumn<Covoitureur, String> nom;
    @FXML
    private TableColumn<Covoitureur, String> prenom;
    @FXML
    private TableColumn<Covoitureur, String> pseudo;
    @FXML
    private TableColumn<Covoitureur, String> nationalite;
    @FXML
    private TableColumn<Covoitureur,Integer> AnneePermis;
    @FXML
    private TableColumn<Covoitureur,Integer  > NSVehicule;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    System.out.println("test1");
         connectionDB c = connectionDB.getInstance();
     Connection con = c.getConnection();
      ArrayList arraylist = null;
        ObservableList<Covoitureur> Covoitureur= FXCollections.observableArrayList();
             try {
           String requete = "select * from Covoitureur inner join membre ";
           Statement st = con.createStatement();
           ResultSet resultset = st.executeQuery(requete);
          // resultset.beforeFirst();
          while(resultset.next()){
          Covoitureur rs;
          // public Covoitureur(String nationalite, int annePermis, int NbSeVehicule, String nom, String adresse, String mail, char role, String psw, String pseudo) {
               Covoitureur.add(new Covoitureur(resultset.getString("nom"),resultset.getString("prenom"),resultset.getString("pseudo"), resultset.getString("nationalite"),resultset.getInt("annePermis"),resultset.getInt("NSvehicule")));
      
           }            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }  
            // CovoitureurServices bs=new CovoitureurServices();
       
       // arraylist = (ArrayList) bs.afficherCovoitureur();
       // ObservableList observableist=FXCollections.observableArrayList(arraylist);
       
     //   col1.setCellValueFactory(new PropertyValueFactory<>("Id"));
    // Covoitureur b = (Covoitureur) TableNut.getSelectionModel().getSelectedItem();
    
      // public Covoitureur(String nationalite, int annePermis, int NbSeVehicule, String nom,String prenom,   String pseudo)
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
                  prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        pseudo.setCellValueFactory(new PropertyValueFactory<>("pseudo"));
        nationalite.setCellValueFactory(new PropertyValueFactory<>("nationalite"));
        //AnneePermis.setCellValueFactory(new PropertyValueFactory<>(Integer.toString(b.getannePermis())));
        AnneePermis.setCellValueFactory(new PropertyValueFactory<>("annePermis"));
        //NSVehicule.setCellValueFactory(new PropertyValueFactory<>(Integer.toString(b.getNbSeVehicule())));
        NSVehicule.setCellValueFactory(new PropertyValueFactory<>("NSvehicule"));
        
      //  colImg.setCellValueFactory(new PropertyValueFactory<>("Nom_image"));
    //  TableNut.getSelectionModel().selectFirst();  
     // Covoitureur b = TableNut.getSelectionModel().getSelectedItem();
      System.out.println("test");
       TableNut.setItems( Covoitureur);
       TableNut.setVisible(true);
      System.out.println("test2");  
  
    }
   

    @FXML
    private void nutdelete(ActionEvent event) {
          
    }    
    @FXML
    private void nutretour(ActionEvent event) {
        
        try {
            Parent SecondView;
            SecondView = (Pane) FXMLLoader.load(getClass().getResource("CovScene.fxml"));
            Scene newScene = new Scene(SecondView);
            Stage currStage = (Stage) nutback.getScene().getWindow();
            currStage.setScene(newScene);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
