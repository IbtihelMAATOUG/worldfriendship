/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class CovSceneController implements Initializable {
    @FXML
    private AnchorPane FctScn;
    @FXML
    private JFXButton btn_cov;
    @FXML
    private JFXButton btn_covb;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void afficheConv(ActionEvent event) {
        try {
            FctScn.getChildren().clear();
   FctScn.getChildren().add(FXMLLoader.load(getClass().getResource("AfficherCovoitureurs.fxml")));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void AffichConvBann(ActionEvent event) {
         try {
            FctScn.getChildren().clear();
   FctScn.getChildren().add(FXMLLoader.load(getClass().getResource("AfficherCovoitureursBanis.fxml")));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
    
}
