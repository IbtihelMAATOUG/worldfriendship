/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class AfficherCovoitureursBanisController implements Initializable {
    @FXML
    private TableView<?> TableNut;
    @FXML
    private TableColumn<?, ?> htreg;
    @FXML
    private TableColumn<?, ?> hpoids;
    @FXML
    private TableColumn<?, ?> hprot;
    @FXML
    private TableColumn<?, ?> hgluc;
    @FXML
    private TableColumn<?, ?> hlip;
    @FXML
    private TableColumn<?, ?> hliq;
    @FXML
    private Button nutremove;
    @FXML
    private Button nutback;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void nutdelete(ActionEvent event) {
    }

    @FXML
    private void nutretour(ActionEvent event) {
        
        try {
            Parent SecondView;
            SecondView = (Pane) FXMLLoader.load(getClass().getResource("CovScene.fxml"));
            Scene newScene = new Scene(SecondView);
            Stage currStage = (Stage) nutback.getScene().getWindow();
            currStage.setScene(newScene);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
