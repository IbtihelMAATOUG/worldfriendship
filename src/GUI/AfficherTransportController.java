/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import config.connectionDB;
import entities.Covoitureur;
import entities.Transport;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import services.TransportService;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class AfficherTransportController implements Initializable {
    
  
    @FXML
    private AnchorPane FctScn;
    @FXML
    private TableView<Transport> htransport;
    @FXML
    private TableColumn<Transport, ?> hcompanie;
    @FXML
    private TableColumn<Transport, ?> htype;
    @FXML
    private TableColumn<Transport, ?> hhoraire;
    @FXML
    private TableColumn<Transport, ?> htaller;
    @FXML
    private TableColumn<Transport, ?> htretour;
    @FXML
    private TableColumn<Transport, ?> hprix;
    @FXML
    private Button helaback;
    @FXML
    private Button helaremove;
    @FXML
    private Button modifcons;
    @FXML
    private TextField search;
    @FXML
    private TextField compagnie;
    @FXML
    private TextField Type;
    @FXML
    private TextField horaire;
    @FXML
    private TextField TrajetAller;
    @FXML
    private TextField TrajetRetour;
    @FXML
    private TextField prix;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         
        //////////////////////////////////////////////////
        System.out.println("test1");
         connectionDB c = connectionDB.getInstance();
     Connection con = c.getConnection();
      ArrayList arraylist = null;
        ObservableList<Transport> Transport= FXCollections.observableArrayList();
             try {
           String requete = "select * from transport ";
           Statement st = con.createStatement();
           ResultSet resultset = st.executeQuery(requete);
          // resultset.beforeFirst();
          while(resultset.next()){
          Covoitureur rs;
          // public Covoitureur(String nationalite, int annePermis, int NbSeVehicule, String nom, String adresse, String mail, char role, String psw, String pseudo) {
               Transport.add(new Transport(resultset.getString("compagnie"),resultset.getString("type"),resultset.getString("horaire"), resultset.getString("trajetAller"),resultset.getString("trajetRetour"),resultset.getInt("prix")));
      
           }            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }  
            // CovoitureurServices bs=new CovoitureurServices();
       
       // arraylist = (ArrayList) bs.afficherCovoitureur();
       // ObservableList observableist=FXCollections.observableArrayList(arraylist);
       
     //   col1.setCellValueFactory(new PropertyValueFactory<>("Id"));
    // Covoitureur b = (Covoitureur) TableNut.getSelectionModel().getSelectedItem();
    
      // public Covoitureur(String nationalite, int annePermis, int NbSeVehicule, String nom,String prenom,   String pseudo)
    hcompanie.setCellValueFactory(new PropertyValueFactory<>("compagnie"));
                  htype.setCellValueFactory(new PropertyValueFactory<>("type"));
        hhoraire.setCellValueFactory(new PropertyValueFactory<>("horaire"));
        htaller.setCellValueFactory(new PropertyValueFactory<>("trajetAller"));
        //AnneePermis.setCellValueFactory(new PropertyValueFactory<>(Integer.toString(b.getannePermis())));
       htretour.setCellValueFactory(new PropertyValueFactory<>("trajetRetour"));
        //NSVehicule.setCellValueFactory(new PropertyValueFactory<>(Integer.toString(b.getNbSeVehicule())));
        hprix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        
      //  colImg.setCellValueFactory(new PropertyValueFactory<>("Nom_image"));
    //  TableNut.getSelectionModel().selectFirst();  
     // Covoitureur b = TableNut.getSelectionModel().getSelectedItem();
      System.out.println("test");
       htransport.setItems( Transport);
       htransport.setVisible(true);
      System.out.println("test2");  
  /////////////////////////////////
     
    }    

    @FXML
    private void returnto(ActionEvent event) {
    }

   /* @FXML
    private void heladelete(ActionEvent event) {
           TransportService ts = new TransportService();
       // System.out.println(companie.getText()+" "+type.getText()+" "+horaire.getText()+" "+taller.getText()+" "+tretour.getText()+" "+Integer.parseInt(prix.getText()));
     
        Transport t = new Transport(compagnie.getText(),Type.getText(),horaire.getText(),TrajetAller.getText(),TrajetRetour.getText(),Integer.parseInt(prix.getText()));
        try {
            ts.ModifierTransport(t);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }*/
    /*@FXML
    private void heladelete(ActionEvent event)throws SQLException, IOException {
        ObservableList<Transport> row, allRows;
        allRows=htransport.getItems();
        row=htransport.getSelectionModel().getSelectedItems();
        System.out.println(row);
        int x=row.get(0).getIdTransport();
        //SupprimerConsultation (x)
        TransportService ts = new TransportService();
        try {
             
            s.SupprimerTransport(x);
            //s2.SupprimerRegime(1);
            System.out.println("transport Supprimé");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
       //StringProperty selectedItem=helatab.getSelectionModel().getSelectedItem().getId_consultation().link1;
        row.forEach(allRows::remove);
        Alert alert=new Alert(Alert.AlertType.INFORMATION);
Stage stage=(Stage) alert.getDialogPane().getScene().getWindow();
stage.initStyle(StageStyle.TRANSPARENT);
//alert.setTitle("Ajout");
alert.setContentText("Suppression Effectué avec Succés");
alert.showAndWait();
Parent root = FXMLLoader.load(getClass().getResource("AfficherTransport.fxml"));


             helaremove.getScene().setRoot(root);
    }
*/

    @FXML
    private void UpdateConsultation(ActionEvent event) {
       String a = htransport.getSelectionModel().getSelectedItem().getHoraire();
        horaire.setText(a);
         String a1 = htransport.getSelectionModel().getSelectedItem().getTrajetAller();
        TrajetAller.setText(a1);
        String a2 = htransport.getSelectionModel().getSelectedItem().getTrajetRetour();
        TrajetRetour.setText(a2);
        String a3 = htransport.getSelectionModel().getSelectedItem().getType();
        Type.setText(a1);
        String aa = htransport.getSelectionModel().getSelectedItem().getCompagnie();
        compagnie.setText(aa);
         int a4 = htransport.getSelectionModel().getSelectedItem().getPrix();
        prix.setText(Integer.toString(a4));
    }
    
}
