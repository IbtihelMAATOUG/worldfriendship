/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import entities.Transport;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import services.TransportService;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class ModifierTransportController implements Initializable {
    @FXML
    private AnchorPane wass;
    @FXML
    private Pane lol;
    @FXML
    private TextField companie;
    @FXML
    private TextField type;
    @FXML
    private TextField horaire;
    @FXML
    private TextField taller;
    @FXML
    private Button helaaffich;
    @FXML
    private Button modifier;
    @FXML
    private TextField tretour;
    @FXML
    private TextField prix;
    @FXML
    private Button decmed;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
       
    }    

    @FXML
    private void AfficherTransport(ActionEvent event) {
    }

    @FXML
    private void ModifierTransport(ActionEvent event) {
           TransportService ts = new TransportService();
        System.out.println(companie.getText()+" "+type.getText()+" "+horaire.getText()+" "+taller.getText()+" "+tretour.getText()+" "+Integer.parseInt(prix.getText()));
     
        Transport t = new Transport(companie.getText(),type.getText(),horaire.getText(),taller.getText(),tretour.getText(),Integer.parseInt(prix.getText()));
        try {
            ts.ModifierTransport(t);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void MedecinDec(ActionEvent event) {
    }
    
}
