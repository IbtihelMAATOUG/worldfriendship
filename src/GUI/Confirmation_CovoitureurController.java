/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class Confirmation_CovoitureurController implements Initializable {

    private DevenirCovoitureurController parent ;
    @FXML
    private Button annuler;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void confirmer_button(ActionEvent event) {
        try {
            parent.doAdd();
            FXMLLoader fxmll = new FXMLLoader(getClass().getResource("pageaccueil_user.fxml"));
            Pane  SecondView = (Pane) fxmll.load();
            Scene newScene = new Scene(SecondView);
            Stage Stage = new Stage();
            Stage.setScene(newScene);
            Stage.show();
            parent.close();
            Stage stage = (Stage) annuler.getScene().getWindow();
            stage.hide();
        } catch (IOException ex) {
            Logger.getLogger(Confirmation_CovoitureurController.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }

    @FXML
    private void annuler_button(ActionEvent event) {
        try {
            FXMLLoader fxmll = new FXMLLoader(getClass().getResource("pageaccueil_user.fxml"));
            Pane  SecondView = (Pane) fxmll.load();
            Scene newScene = new Scene(SecondView);
            Stage Stage = new Stage();
            Stage.setScene(newScene);
            Stage.show();
            parent.close();
            
            Stage stage = (Stage) annuler.getScene().getWindow();
            stage.hide();
        } catch (IOException ex) {
            Logger.getLogger(Confirmation_CovoitureurController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DevenirCovoitureurController getParent() {
        return parent;
    }

    public void setParent(DevenirCovoitureurController parent) {
        this.parent = parent;
    }
    
    
}
