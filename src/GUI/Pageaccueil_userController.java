/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class Pageaccueil_userController implements Initializable {
    @FXML
    private AnchorPane pagina;
    @FXML
    private Button transport;
    @FXML
    private Button devenirCovoitureur;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    @FXML
    private void dcnx_accueil(ActionEvent event) {
    }

    @FXML
    private void transport(ActionEvent event) {
        try {
            Parent SecondView;
            SecondView = (Pane) FXMLLoader.load(getClass().getResource("Mapv.fxml"));
            Scene newScene = new Scene(SecondView);
            Stage currStage = (Stage) transport.getScene().getWindow();
            currStage.setScene(newScene);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void devenirCovoitureur(ActionEvent event) {
        try {
            Parent SecondView;
            SecondView = (Pane) FXMLLoader.load(getClass().getResource("DevenirCovoitureur.fxml"));
            Scene newScene = new Scene(SecondView);
            Stage currStage = (Stage) devenirCovoitureur.getScene().getWindow();
            currStage.setScene(newScene);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
