/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import entities.Covoitureur;
import entities.Transport;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import services.CovoitureurServices;
import services.TransportService;

/**
 * FXML Controller class
 *
 * @author azuke
 */
public class DevenirCovoitureurController implements Initializable {

    @FXML
    private Button nutdec;
    @FXML
    private Button ajoutnut;
    @FXML
    private TextField NbSeVehicule;
    @FXML
    private TextField nationalite;
    @FXML
    private TextField annePermis;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void NutriDec(ActionEvent event) {
        try {
            Parent SecondView;
            SecondView = (Pane) FXMLLoader.load(getClass().getResource("DevenirCovoitureur.fxml"));
            Scene newScene = new Scene(SecondView);
            Stage currStage = (Stage) ajoutnut.getScene().getWindow();
            currStage.setScene(newScene);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void AjouterCovoitureur(ActionEvent event) {
        if(Integer.parseInt(annePermis.getText())>2)
           this.confirmWindowCovoitureur();
        else
            this.youareNotAllowed();
      
    }
    private void youareNotAllowed(){
        try {
            Parent SecondView;
            FXMLLoader fxmll = new FXMLLoader(getClass().getResource("Interdir_Covoitureur.fxml"));
            SecondView = (Pane) fxmll.load();
            Interdir_CovoitureurController controller = fxmll.<Interdir_CovoitureurController>getController();
            controller.setParent(this);
            Scene newScene = new Scene(SecondView);
            Stage Stage = new Stage();
            Stage.setScene(newScene);
            Stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    private void confirmWindowCovoitureur(){
          
        try {
            Parent SecondView;
            FXMLLoader fxmll = new FXMLLoader(getClass().getResource("confirmation_Covoitureur.fxml"));
            SecondView = (Pane) fxmll.load();
            Confirmation_CovoitureurController controller = fxmll.<Confirmation_CovoitureurController>getController();
            controller.setParent(this);
            Scene newScene = new Scene(SecondView);
            Stage Stage = new Stage();
            Stage.setScene(newScene);
            Stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public void doAdd(){
        
         CovoitureurServices c = new CovoitureurServices();
        System.out.println(nationalite.getText()
                +" "+annePermis.getText()+
                " "+NbSeVehicule.getText()+" ");
     
        Covoitureur cov = new Covoitureur(nationalite.getText(),Integer.parseInt(annePermis.getText()),Integer.parseInt(NbSeVehicule.getText()));
        try {
           c.AjouterCovoitureur(cov);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    
    }

    public void close() {
        Stage stage = (Stage) ajoutnut.getScene().getWindow();
             stage.hide();
    }

}
