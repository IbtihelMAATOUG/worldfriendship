package services;

import config.connectionDB;
import entities.Covoitureur;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Yassine
 */
public class CovoitureurServices {
      connectionDB c = connectionDB.getInstance();
     Connection con = c.getConnection();
       public CovoitureurServices(){}
         //INSERTTON  
    public void AjouterCovoitureur(Covoitureur cov) throws SQLException{
        String nationalite =cov.getnationalite();
        int annePermis = cov.getannePermis();
        
        int NbSeVehicule = cov.getNSvehicule();
       
       

        try{
      String requete = "insert into Covoitureur(nationalite, annePermis, NSVehicule) VALUES(?,?,?)";
      PreparedStatement st = con.prepareStatement(requete);
      st.setString(1,nationalite );
      st.setInt(2, annePermis);
      st.setInt(3, NbSeVehicule);
     


      st.execute();
      System.out.println("Covoitureur ajoutée");
       }catch(SQLException ex){
        System.out.println(ex.getMessage());
       }
     
    }
  //INSERTTON  
    
    
       //UPDATE 
   public void ModifierCovoitureur(Covoitureur cov) throws SQLException{
       
        String nationalite =cov.getnationalite();
        int annePermis = cov.getannePermis();
        
        int NbSeVehicule = cov.getNSvehicule();
        
        
       
      try{
      String requete = "UPDATE `Covoitureur` SET`nationalite`=? ,`annePermis`=?,`NSVehicule`=?WHERE id=?";
      PreparedStatement st = con.prepareStatement(requete); 
       st.setString(1,nationalite );
      st.setInt(2, annePermis);
      st.setInt(3, NbSeVehicule);
      
      
      st.execute();   
      System.out.println("Covoitureur modifée");
      }catch(SQLException ex){
        System.out.println(ex.getMessage());
      }   
   }  
   //UPDATE
      //DELETE 
     public void supprimerCovoitureur(int id)throws SQLException{
       try{       
        String requete = "delete from covoitureur where NSVehicule = ?";
        PreparedStatement st = con.prepareStatement(requete);
        st.setInt(1, id);
        st.execute();
        System.out.println("Covoitureur supprimée");
     }catch(SQLException ex){
   System.out.println(ex.getMessage());
     }
     }
   //DELETE
     
            //AFFICHAGE
      public ArrayList<Covoitureur> afficherCovoitureur(){
           ArrayList<Covoitureur> Covoitureur = new ArrayList();
             try {
           String requete = "select * from Covoitureur inner join membre ";
           Statement st = con.createStatement();
           ResultSet resultset = st.executeQuery(requete);
           resultset.beforeFirst();
          while(resultset.next()){
          Covoitureur rs;
          // public Covoitureur(String nationalite, int annePermis, int NbSeVehicule, String nom, String adresse, String mail, char role, String psw, String pseudo) {
               rs = new Covoitureur(resultset.getString(2),resultset.getInt(3),resultset.getInt(4),resultset.getString(6),resultset.getString(7),resultset.getString(8));
          Covoitureur.add(rs);
           }            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }  
      return Covoitureur;
      }
    
    //AFFICHAGE

   
}
