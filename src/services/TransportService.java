/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import config.connectionDB;
import entities.Transport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Yassine
 */
public class TransportService {
      connectionDB c = connectionDB.getInstance();
     Connection con = c.getConnection();
     
         //INSERTTON  
    public void AjouterTransport(Transport trans) throws SQLException{
        String compagnie = trans.getCompagnie();
        String type = trans.getType();
        String horaire = trans.getHoraire();
        String TrajetAller = trans.getTrajetAller();
        String TrajetRetour = trans.getTrajetRetour();
        int  prix = trans.getPrix();

        try{
      String requete = "insert into transport(compagnie,type,horaire,trajetAller,trajetRetour,prix) VALUES(?,?,?,?,?,?)";
      PreparedStatement st = con.prepareStatement(requete);
      st.setString(1, compagnie);
      st.setString(2, type);
      st.setString(3, horaire);
      st.setString(4, TrajetAller);
      st.setString(5, TrajetRetour);
      st.setInt(6,prix);


      st.execute();
      System.out.println("transport ajoutée");
       }catch(SQLException ex){
        System.out.println(ex.getMessage());
       }
     
    }
  //INSERTTON  
    
    
       //UPDATE 
   public void ModifierTransport(Transport trans) throws SQLException{
      String compagnie =trans.getCompagnie();
        String Type = trans.getType();
        String horaire = trans.getHoraire();
        String trajetAller = trans.getTrajetAller();
        String trajetRetour = trans.getTrajetRetour();
        
        
        int prix = trans.getPrix();
        
         int id = trans.getId();
       
      try{
      String requete = "UPDATE `transport` SET`compagnie`=? ,`Type`=?,`horaire`=?,`trajetAller`=?,`trajetRetour`=?,`prix`=?WHERE id=?";
      PreparedStatement st = con.prepareStatement(requete); 
       st.setString(1,compagnie );
      st.setString(2, Type);
      st.setString(3, horaire);
      st.setString(4, trajetAller);
      st.setString(5, trajetRetour);
      st.setInt(6, prix);
      st.setInt(7, id);
      
      
      
      st.execute();   
      System.out.println("Transport modifée");
      }catch(SQLException ex){
        System.out.println(ex.getMessage());
      }   
   }  
   //UPDATE
      //DELETE 
     public void supprimerTransport(int id)throws SQLException{
       try{       
        String requete = "delete from transport where id = ?";
        PreparedStatement st = con.prepareStatement(requete);
        st.setInt(1, id);
        st.execute();
        System.out.println("transport supprimée");
     }catch(SQLException ex){
   System.out.println(ex.getMessage());
     }
     }
   //DELETE
     
            //AFFICHAGE
      public ArrayList<Transport> afficherTransport()throws SQLException{
           ArrayList<Transport> Transport = new ArrayList();
             try {
           String requete = "select * from transport";
           Statement st = con.createStatement();
           ResultSet resultset = st.executeQuery(requete);
           resultset.beforeFirst();
          while(resultset.next()){
          Transport rs = new Transport(resultset.getString(2),resultset.getString(3),resultset.getString(4),resultset.getString(5),resultset.getString(6),resultset.getInt(7));
          Transport.add(rs);
           }            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }  
      return Transport;
      }
    
    //AFFICHAGE

   
}
