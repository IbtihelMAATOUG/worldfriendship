/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

/**
 *
 * @author Yassine
 */
public class Transport extends RecursiveTreeObject<Transport> {
      String compagnie;
      String type;
      String horaire;
      String TrajetAller;
      String TrajetRetour;
      int prix;
      int id ;

    public Transport() {
    }

      
    public Transport(String compagnie, String type, String horaire, String TrajetAller,String TrajetRetour, int prix) {
        this.compagnie = compagnie;
        this.type = type;
        this.horaire = horaire;
        this.TrajetAller = TrajetAller;
        this.TrajetRetour = TrajetRetour;
        this.prix = prix;
    }

    public Transport(String compagnie, String type, String horaire, String TrajetAller, String TrajetRetour, int prix, int id) {
        this.compagnie = compagnie;
        this.type = type;
        this.horaire = horaire;
        this.TrajetAller = TrajetAller;
        this.TrajetRetour = TrajetRetour;
        this.prix = prix;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

  

    public String getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(String compagnie) {
        this.compagnie = compagnie;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHoraire() {
        return horaire;
    }

    public void setHoraire(String horaire) {
        this.horaire = horaire;
    }

    public String getTrajetAller() {
        return TrajetAller;
    }

    public void setTrajetAller(String TrajetAller) {
        this.TrajetAller = TrajetAller;
    }

    public String getTrajetRetour() {
        return TrajetRetour;
    }

    public void setTrajetRetour(String TrajetRetour) {
        this.TrajetRetour = TrajetRetour;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Transport{" + "compagnie=" + compagnie + ", type=" + type + ", horaire=" + horaire + ", TrajetAller=" + TrajetAller + ", TrajetRetour=" + TrajetRetour + ", prix=" + prix + ", id=" + id + '}';
    }

   
      
}
