/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author azuke
 */
public class Membre {
    
    private int ID;
    private String nom;
    private String prenom;
    private String mail;
    private char role;
    private String psw;
    private String pseudo;

    public Membre() {
    }
    
    

    public Membre(String nom,String prenom,  String mail, char role, String psw, String pseudo) {
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.role = role;
        this.psw = psw;
        this.pseudo = pseudo;
    }
 public Membre(String nom,String prenom, String pseudo) {
        this.nom = nom;
        
       this.prenom=prenom;
        this.pseudo = pseudo;
    }

    
    
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
 public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
   

    

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public char getRole() {
        return role;
    }

    public void setRole(char role) {
        this.role = role;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    @Override
    public String toString() {
        return "Membre{" + "ID=" + ID + ", nom=" + nom +  ", mail=" + mail + ", role=" + role + ", psw=" + psw + ", pseudo=" + pseudo + '}';
    }
    
    
    
}
