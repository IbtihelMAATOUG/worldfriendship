/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author azuke
 */
public class Covoitureur extends Membre {
    private String nationalite;
    private int annePermis;
    
    private int NSvehicule;
    private boolean banis;
    

    public Covoitureur() {
    }

    public Covoitureur(String nationalite , int annePermis,int NSvehicule) {
        this.nationalite = nationalite;
        this.annePermis = annePermis;
        this.NSvehicule = NSvehicule;
      
    }

    public Covoitureur(String nationalite, int annePermis, int NSvehicule, String nom,String prenom,   String pseudo) {
        super(nom , prenom, pseudo);
        this.nationalite = nationalite;
        this.annePermis = annePermis;
        this.NSvehicule = NSvehicule;
    }
    public Covoitureur(String nom,String prenom,   String pseudo,String nationalite, int annePermis, int NSvehicule, boolean banis) {
        super(nom , prenom, pseudo);
        this.nationalite = nationalite;
        this.annePermis = annePermis;
        this.NSvehicule = NSvehicule;
        this.banis = banis;
    }
    
public Covoitureur(String nom,String prenom,   String pseudo,String nationalite, int annePermis, int NSvehicule) {
        super(nom , prenom, pseudo);
        this.nationalite = nationalite;
        this.annePermis = annePermis;
        this.NSvehicule = NSvehicule;
        
    }
    public boolean isBanis() {
        return banis;
    }

    public void setBanis(boolean banis) {
        this.banis = banis;
    }
    
    public String getnationalite(){
        return nationalite;
    }

    public int getannePermis() {
        return annePermis;
    }
    public int getNSvehicule() {
        return NSvehicule;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public void setAnnePermis(int annePermis) {
        this.annePermis = annePermis;
    }

    public void setNSvehicule(int NSvehicule) {
        this.NSvehicule = NSvehicule;
    }

    @Override
    public String toString() {
        return "Covoitureur{" + "nationalite=" + nationalite + ", annePermis=" + annePermis + ", NbSeVehicule=" + NSvehicule + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.NSvehicule;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Covoitureur other = (Covoitureur) obj;
        if (this.NSvehicule != other.NSvehicule) {
            return false;
        }
        return true;
    }
    

    

    

   

   

   
    
    
    
    
    
    
    
}
